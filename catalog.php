<!DOCTYPE html>
<head>
	<title>Product list</title>
	<meta charset="UTF-8">
	<!-- Page formatting-->
	<link rel="stylesheet" type="text/css" href="css/catalogDesign.css">
	<!-- Add bootrstrap CDN -->
	<!-- CSS only -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<!-- JS, Popper.js, and jQuery -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- Fit page in all devices-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
</head>

<body>
	<?php
		include "include/main.php";
	?>
	<div class="container">
		<div class="row" id="titleRow">
			<div class="col-sm-8">
				<p id="title"> Product list </p>
			</div>
			<div class="col-sm-3">
				<p id="deleteLabel"> Mass Delete Action: <p>
			</div>
			<div class="col-sm-1">
				<button id="deleteBtn" type="submit" form="catalogFrm"> Apply </button>
			</div>
		</div>
		<hr id="line">
		<div class="card-columns">
		<form id="catalogFrm" action="include/main.php" method="post">
		<?php
		$productArr = Product::getAllProducts();
		foreach($productArr as $item){
			//var_dump($item);?>
			<div class="card">
				<div class="card-body">
					<input name="productId[]" class="deleteCheckbox" type="checkbox" value="<?php echo $item['id']; ?>" >
					<h5 class="card-title"><?php echo $item['sku']; ?></h5>
					<p class="card-text"><?php echo $item['name']; ?></p>
					<p class="card-text"><?php echo $item['price']." $"; ?></p>
					<p class="card-text">
					<?php 
						if(isset($item['size']))
							echo "Size: ".$item['size']." MB";
						else if (isset($item['weight']))
							echo "Weight: ".$item['weight']." KG";
						else if (isset($item['dimensions']))
							echo "Dimensions: ".$item['dimensions'];
					?>
					</p>
				</div>
			</div>
			<br>
		<?php } ?>
		</form>
		</div>
	</div>
</body>


