<!DOCTYPE html>
<head>
	<title>Product new </title>
	<meta charset="UTF-8">
	<!-- Page formatting-->
	<link rel="stylesheet" type="text/css" href="css/addDesign.css">
	<!-- Add bootrstrap CDN -->
	<!-- CSS only -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<!-- JS, Popper.js, and jQuery -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<!-- Fit page in all devices-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<!--Additional scripts -->
	<script src="js/productAdd.js"></script>
</head>

<body>
	<?php
		include "include/main.php";
	?>
	<div class="container">
		<div class="row" id="titleRow">
			<div class="col-sm-8">
				<p id="title" > Product add </p>
			</div>
			<div class="col-sm-4">
				<button type="submit" id="btnSave" form="addFrm"> Save </button>
			</div>
			<hr id="line">
		</div>
		<div class="row" >
			<form id="addFrm" action="include/main.php" method="post">
				<table>
				<tr>
					<td>
						<label>SKU:</label> 
					</td>
					<td>
						<input type="text" id="sku" name="sku" required> 
					</td>
				</tr>
				<tr>
					<td>
						<label >Name:</label> 
					</td>
					<td>
						<input type="text" id="name" name="name" required>
					</td>
				</tr>
				<tr>
					<td>
						<label >Price:</label> 
					</td>
					<td>
						<input type="number" id="price" name="price" required > 
					</td>
				</tr>
				<tr>
					<td>
						<label for="productType">Type switcher:</label> 
					</td>
					<td>
						<select id="typeSwitcher" name = "productType" onChange="chosenOption()" > 
							<option selected="selected" disabled >Select product type </option>
							<option value="DVD_disc"> DVD </option>
							<option value="Book"> Book </option>
							<option value="Furniture"> Furniture </option>
						</select>
					</td>
				</tr>
				<tr id="additionalFormField">
				</tr>
				</table>
			</form>
		</div>
	</div>
</body>


