//Function for dynamic form changes
function chosenOption(){
	var option = document.getElementById("typeSwitcher").value;
	var xhttp = new XMLHttpRequest();
	//alert(option);
	switch(option){
		case "DVD_disc":{
			document.getElementById("additionalFormField").innerHTML = "";
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					document.getElementById("additionalFormField").innerHTML = this.responseText;
				}
			};
			xhttp.open("POST", "include/dvdForm.php",true);
			xhttp.send();
			break;
		}
		case "Book":{
			document.getElementById("additionalFormField").innerHTML = "";
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					document.getElementById("additionalFormField").innerHTML = this.responseText;
				}
			};
			xhttp.open("POST", "include/bookForm.php",true);
			xhttp.send();
			
			break;
		}
		case "Furniture":{
			document.getElementById("additionalFormField").innerHTML = "";
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					document.getElementById("additionalFormField").innerHTML = this.responseText;
				}
			};
			xhttp.open("POST", "include/furnitureForm.php",true);
			xhttp.send();
			
			break;
		}
		default:
			alert("File does not exist!!");
	}
}

//Function for furniture dimesions correct formating
function setDimensions(){
	var dimensions;
	var length = document.getElementById("length").value;
	var width = document.getElementById("width").value;
	var height = document.getElementById("height").value;
	
	dimensions = height +"x"+width+"x"+length;
	document.getElementById("addProperty").value = dimensions;
	//alert(dimensions);
}
