-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2020 at 09:21 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catalog`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `size` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `dimensions` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `size`, `weight`, `dimensions`) VALUES
(18, '23dvd78x', 'Acme DVD', 3, 80, NULL, NULL),
(23, 'BOK23uv', 'Educated elephant', 6, NULL, 1, NULL),
(24, 'BOK34axc', 'A Spark of Light', 4, NULL, 2, NULL),
(25, 'BOK67kc', 'The Library Book', 5, NULL, 3, NULL),
(26, 'FUR23v', 'Table', 34, NULL, NULL, '150x100x200'),
(27, 'FUR34xc', 'Chair', 67, NULL, NULL, '70x50x150'),
(28, 'FUR56un', 'Sofa', 400, NULL, NULL, '100x150x200'),
(29, 'DVD45uu', 'Acme Disc', 4, 60, NULL, NULL),
(31, '345Book', 'War and peace', 56, NULL, 32, NULL),
(34, '78DVD', 'Google DVD', 45, 56, NULL, NULL),
(35, 'BOK84gg', 'Gone with the Wind', 3, NULL, 1, NULL),
(37, 'FUR12zq', 'Lamp', 12, NULL, NULL, '100x34x170'),
(38, 'DVD45zc', 'Kind of disc', 23, 56, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
